<?php
/**
 * @file
 * octopress.features.inc
 */

/**
 * Implements hook_views_api().
 */
function octopress_views_api() {
  return array("version" => "3.0");
}
